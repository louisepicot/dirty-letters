let scene = new THREE.Scene(),
  renderer = window.WebGLRenderingContext
    ? new THREE.WebGLRenderer({ alpha: true })
    : new THREE.CanvasRenderer(),
  light = new THREE.AmbientLight(0xffffff),
  camera,
  controls,
  root,
  objLoader;

const init = () => {
  renderer.setSize(window.innerWidth, window.innerHeight);
  renderer.setClearColor(0xffffff, 0);
  document.getElementById("three-container").appendChild(renderer.domElement);
  scene.add(light);

  camera = new THREE.PerspectiveCamera(
    35,
    window.innerWidth / window.innerHeight,
    1,
    1000
  );
  camera.position.set(5, 5, 5);
  camera.lookAt(scene.position);
  scene.add(camera);

  //    const cubeGeometry = new THREE.BoxGeometry(10, 10, 10);
  //    const cubeMaterial = new THREE.MeshBasicMaterial({ color: 0xff2d00  });
  //    const cube = new THREE.Mesh(cubeGeometry, cubeMaterial);

  //     cube.position.set(20, 20, 20);
  //     cube.name = "cube"
  //     scene.add(cube);

  objLoader = new THREE.OBJLoader2();
  objLoader.loadMtl("./A2.mtl", null, (materials) => {
    objLoader.setMaterials(materials);
    objLoader.load("./A2.obj", (event) => {
      console.log(event);
      root = event.detail.loaderRootNode;
      root.position.set(0, 0, 0);
      root.scale.set(4, 4, 4);
      root.name = "object";
      scene.add(root);
    });
  });

  console.log("obj", scene.getObjectByName("object"));
  controls = new THREE.OrbitControls(camera, renderer.domElement);
  // controls.movementSpeed = 0.005;
  // controls.domElement = document;
  // controls.rollSpeed = 0.0001;
  // controls.autoForward = false;
  // controls.maxZoom = 100

  render();
};

const render = () => {
  // TWEEN.update();
  if (scene.getObjectByName("object")) {
    const object = scene.getObjectByName("object");
    object.rotation.y += 0.0003;
  }
  controls.update();
  renderer.render(scene, camera);
  requestAnimationFrame(render);
};

window.onload = init();
